# opm-debian.mk needs to included first!
# Only include for packages with a libopm-<project> library package.

override_dh_makeshlibs:
	dh_makeshlibs -plib$(DEB_SOURCE) --version-info='$(OPM_DEBIAN_SHLIB)'
