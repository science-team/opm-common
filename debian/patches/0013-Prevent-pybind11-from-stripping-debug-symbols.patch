From: Markus Blatt <markus@dr-blatt.de>
Date: Wed, 24 Apr 2024 15:18:11 +0200
Subject: Prevent pybind11 from stripping debug symbols

 pybind11_add_module will strip debug symbols if CMAKE_BUILD_TYPE is
 neither Debug nor RelWithDebInfo) and there is no option to prevent
 this. To fix this we temporarily reset the CMAKE_BUILD_TYPE to Debug

We noticed the problem in the build logs:

   dh_dwz -a -O--max-parallel=2
	dwz -- debian/libopm-common/usr/lib/x86_64-linux-gnu/libopmcommon.so.2024.04
	install -m0755 -d debian/python3-opm-common/usr/lib/debug/.dwz/x86_64-linux-gnu
	dwz -mdebian/python3-opm-common/usr/lib/debug/.dwz/x86_64-linux-gnu/python3-opm-common.debug -M/usr/lib/debug/.dwz/x86_64-linux-gnu/python3-opm-common.debug -- debian/python3-opm-common/usr/lib/python3/dist-packages/opm/opmcommon_python.cpython-311-x86_64-linux-gnu.so debian/python3-opm-common/usr/lib/python3/dist-packages/opm_embedded.cpython-311-x86_64-linux-gnu.so
	install -m0755 -d debian/libopm-common-bin/usr/lib/debug/.dwz/x86_64-linux-gnu
	dwz -mdebian/libopm-common-bin/usr/lib/debug/.dwz/x86_64-linux-gnu/libopm-common-bin.debug -M/usr/lib/debug/.dwz/x86_64-linux-gnu/libopm-common-bin.debug -- debian/libopm-common-bin/usr/bin/arraylist debian/libopm-common-bin/usr/bin/co2brinepvt debian/libopm-common-bin/usr/bin/compareECL debian/libopm-common-bin/usr/bin/convertECL debian/libopm-common-bin/usr/bin/hysteresis debian/libopm-common-bin/usr/bin/opmhash debian/libopm-common-bin/usr/bin/opmpack debian/libopm-common-bin/usr/bin/rst_deck debian/libopm-common-bin/usr/bin/summary
dwz: debian/python3-opm-common/usr/lib/python3/dist-packages/opm/opmcommon_python.cpython-311-x86_64-linux-gnu.so: .debug_info section not present
dwz: debian/python3-opm-common/usr/lib/python3/dist-packages/opm_embedded.cpython-311-x86_64-linux-gnu.so: .debug_info section not present
---
 CMakeLists.txt | 6 ++++++
 1 file changed, 6 insertions(+)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index 1e7ae6b..98415a4 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -426,12 +426,18 @@ if (OPM_ENABLE_PYTHON)
   file(COPY ${PROJECT_SOURCE_DIR}/python/README.md ${PROJECT_SOURCE_DIR}/python/MANIFEST.in
        DESTINATION ${PROJECT_BINARY_DIR}/python)
 
+  # pybind11 will always strip debug symbols if CMAKE_BUILD_TYPE is empty
+  # We simply reset here to Debug to prevent stripping.
+  set(OPM_CMAKE_BUILD_TYPE "${CMAKE_BUILD_TYPE}")
+  set(CMAKE_BUILD_TYPE Debug)
   pybind11_add_module(opmcommon_python
                       ${PYTHON_CXX_SOURCE_FILES}
                       ${PROJECT_BINARY_DIR}/python/cxx/builtin_pybind11.cpp)
 
   target_link_libraries(opmcommon_python PRIVATE
                         opmcommon)
+  set(CMAKE_BUILD_TYPE "${OLD_CMAKE_BUILD_TYPE}")
+
   if(TARGET pybind11::pybind11)
     target_link_libraries(opmcommon_python PRIVATE
                           pybind11::pybind11)
