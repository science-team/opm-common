Source: opm-common
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Arne Morten Kvarving <arne.morten.kvarving@sintef.no>,
           Markus Blatt <markus@dr-blatt.de>
Section: libs
Priority: optional
Build-Depends: cmake,
               mpi-default-bin,
               mpi-default-dev,
               bc,
               procps,
               debhelper-compat (= 13),
               libcjson-dev,
               libfmt-dev,
               quilt,
               dh-sequence-python3,
               pkgconf,
               lsb-release,
               libtool,
               doxygen,
               graphviz,
               texlive-latex-extra,
               texlive-latex-recommended,
               ghostscript,
               libboost-system-dev,
               libboost-test-dev,
               zlib1g-dev,
               python3-dev,
               libpython3-dev,
               python3-numpy,
               python3-setuptools,
               python3-setuptools-scm,
               python3-pytest-runner,
               python3-decorator,
               libdune-common-dev (>= 2.10.0~),
               pybind11-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/science-team/opm-common
Vcs-Git: https://salsa.debian.org/science-team/opm-common.git
Homepage: https://opm-project.org
Rules-Requires-Root: no

Package: libopm-common
Architecture: amd64 arm64 armel ia64 loong64 m68k mips64el mipsel ppc64el riscv64
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends}
Pre-Depends: ${misc:Pre-Depends}
Provides: ${opm:shared-library}
Replaces: libopm-common1
Description: Tools for Eclipse reservoir simulation files -- library
 The Open Porous Media (OPM) software suite provides libraries and
 tools for modeling and simulation of porous media processes, especially
 for simulating CO2 sequestration and improved and enhanced oil recovery.
 .
 The Eclipse file format is widely used in the reservoir simulation
 community. This package provides library containing code for processing
 files in Eclipse format as well as utility code used by other OPM
 modules.

Package: libopm-common-bin
Architecture: amd64 arm64 armel ia64 loong64 m68k mips64el mipsel ppc64el riscv64
Multi-Arch: foreign
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Replaces: libopm-common1-bin
Description: Tools for Eclipse reservoir simulation files -- utility programs
 The Open Porous Media (OPM) software suite provides libraries and
 tools for modeling and simulation of porous media processes, especially
 for simulating CO2 sequestration and improved and enhanced oil recovery.
 .
 The Eclipse file format is widely used in the reservoir simulation
 community. This package provides utility programs to process files
 in this format, like for comparing two files, converting between
 binary version and formatted text file format, etc.

Package: libopm-material-dev
Architecture: all
Section: oldlibs
Depends: libopm-common-dev (>= 2024.10~),
         ${misc:Depends}
Description: transitional package
 This is a transitional package. It can safely be removed.

Package: libopm-common-dev
Architecture: amd64 arm64 armel ia64 loong64 m68k mips64el mipsel ppc64el riscv64
Multi-Arch: same
Section: libdevel
Depends: ${opm:shared-library},
         ${misc:Depends},
         libcjson-dev,
         libfmt-dev,
         libboost-system-dev,
         libpython3-dev,
         libdune-common-dev
Suggests: libopm-common-doc
Breaks: libopm-material-dev (<< 2023.04~)
Replaces: libopm-common1-dev,
          libopm-material-dev (<< 2023.04~),
          libopm-material1-dev
Description: Tools for Eclipse reservoir simulation files -- development files
 The Open Porous Media (OPM) software suite provides libraries and
 tools for modeling and simulation of porous media processes, especially
 for simulating CO2 sequestration and improved and enhanced oil recovery.
 .
 This package contains the shared buildsystem of all OPM modules,
 the headers for input, parsing, and output of files in Eclipse format,
 a format widely used in the reservoir simulation community, and generic
 utilities used in other OPM modules.

Package: libopm-material-doc
Architecture: all
Section: oldlibs
Depends: libopm-common-doc (>= 2023.04~),
         ${misc:Depends}
Description: transitional package
 This is a transitional package. It can safely be removed.

Package: libopm-common-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Breaks: libopm-material-doc (<< 2023.04~)
Replaces: libopm-common1-doc,
          libopm-material-doc (<< 2023.04~),
          libopm-material1-doc
Description: Tools for Eclipse reservoir simulation files -- documentation
 The Open Porous Media (OPM) software suite provides libraries and
 tools for modeling and simulation of porous media processes, especially
 for simulating CO2 sequestration and improved and enhanced oil recovery.
 .
 This package contains the source code documentation of library and tools
 for Eclipse reservoir simulation files.

Package: python3-opm-common
Architecture: amd64 arm64 armel ia64 loong64 m68k mips64el mipsel ppc64el riscv64
Section: python
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends},
         libopm-common,
         python3-numpy,
         python3-decorator
Pre-Depends: ${misc:Pre-Depends}
Description: Tools for Eclipse reservoir simulation files -- Python wrappers
 The Open Porous Media (OPM) software suite provides libraries and
 tools for modeling and simulation of porous media processes,
 especially for simulating CO2 sequestration and improved and enhanced
 oil recovery.
 .
 This package contains the Python wrappers for reading, parsing, and
 writing files in Eclipse format, a format widely used in the
 reservoir simulation community.
