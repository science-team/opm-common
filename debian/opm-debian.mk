include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/pkg-info.mk
include /usr/share/dune/dune-debian.env

LSB_RELEASE = $(shell lsb_release -d | sed "s/.*:\s\+\(.*\)/\1/")
# Needed for reproducable builds as we use __FILE__
export DEB_BUILD_MAINT_OPTIONS += reproducible=+fixfilepath
OPM_DEBIAN_CMAKE_FLAGS += -DBUILD_SHARED_LIBS=1 -DCMAKE_INSTALL_DOCDIR=share/doc/lib$(DEB_SOURCE) -DPYTHON_INSTALL_PREFIX=lib/python3/dist-packages -DOPM_INSTALL_COMPILED_PYTHON=OFF -DUSE_RUNPATH=OFF -DWITH_NATIVE=OFF -DUSE_MPI=ON -DUSE_BASH_COMPLETIONS_DIR=ON -DOPM_BINARY_PACKAGE_VERSION="$(LSB_RELEASE): $(DEB_VERSION)"

OPM_DEBIAN_SHLIB = $(subst ~,.,lib$(DEB_SOURCE)-$(DEB_VERSION_UPSTREAM:+ds=))

override_dh_auto_configure:
	dh_auto_configure -- $(OPM_DEBIAN_CMAKE_FLAGS)

override_dh_auto_install:
	dh_auto_install -- install-html

override_dh_gencontrol:
	dh_gencontrol -- -Vopm:shared-library='$(OPM_DEBIAN_SHLIB)'

override_dh_prep:
	dh_prep
	# remove __pycache__ from dh_test run
	py3clean obj-*/python
